scala (2.11.12-4) unstable; urgency=medium

  * Team upload.
  * Preserve the compatibility with Java 8 when compiling with Java 11
  * Removed the get-orig-source target in debian/rules
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 16 Nov 2018 00:45:01 +0100

scala (2.11.12-3) unstable; urgency=medium

  * Team upload.

  [ Markus Koschany ]
  * Add 0017-bug912393.patch and fix several compilation errors.
    (Closes: #912393)
  * Declare compliance with Debian Policy 4.2.1.

  [ Emmanuel Bourg ]
  * Depend on scala (>= 2.11.12) to build with Java 9

 -- Markus Koschany <apo@debian.org>  Sat, 10 Nov 2018 16:14:38 +0100

scala (2.11.12-2) unstable; urgency=medium

  * Team upload.
  * Remove temporary build-dependency on openjdk-8-jdk and build with Java 9.
    (Closes: #895675)
  * Declare compliance with Debian Policy 4.1.4.

 -- Markus Koschany <apo@debian.org>  Mon, 23 Apr 2018 11:05:07 +0200

scala (2.11.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Fixes the Java 9 compatibility at runtime (Closes: #873705)
    - Patched the project to build with Java 9
    - Disabled the boot classpath by default (-nobootcp) to run with Java 9
    - Temporarily build with Java 8 to bootstrap the Java 9 compatible version
    - Refreshed the patches
    - Depend on libjline2-java (>= 2.13)
    - Depend on scala-asm (>= 5.2.0-scala-2)
    - Disabled jarlister during the build (not in Debian)
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 30 Mar 2018 16:17:53 +0200

scala (2.11.8-2) unstable; urgency=medium

  * Team upload.

  [ Mehdi Dogguy ]
  * Remove myself from Uploaders

  [ Emmanuel Bourg ]
  * Build with the Aether Ant tasks instead of the Maven Ant tasks
  * Added the missing Maven rule for jline (Closes: #853733)
  * Switch to debhelper level 10
  * Standards-Version updated to 4.1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 21 Sep 2017 10:38:39 +0200

scala (2.11.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Refreshed the patches
    - New dependency on scala-asm
    - New build dependency on libjarjar-java
    - Use the 'debian' version of scala-xml and scala-parser-combinators
  * Fixed the GitHub links in the API documentation
  * Replaced the Git date/hash in the osgi version with 'unknown'
  * Removed 0002-Use-system-ant-contrib.jar.patch (ant-contrib is already
    on the Ant classpath)
  * debian/rules: Remove the log files left by the test suite
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL
  * Improved the description for scala-library and scala-doc

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 08 Aug 2016 13:49:13 +0200

scala (2.11.6-6) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with bnd > 2.1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 21 Dec 2015 15:41:47 +0100

scala (2.11.6-5) unstable; urgency=medium

  * Team upload.
  * Use scala's version of jquery and jquery-ui.
    Revert the switch to Debian's version of jquery and jquery-ui because the
    documentation would be less useful otherwise.

 -- Markus Koschany <apo@debian.org>  Wed, 16 Dec 2015 20:11:30 +0100

scala (2.11.6-4) unstable; urgency=medium

  * Team upload.
  * debian/copyright: Add the missing Apache 2.0 licensed files.
    Thanks to Thorsten Alteholz for the report. (Closes: #808045)
  * Install all Scala API documentation. (Closes: #800376)
    Thanks to Taran Lynn for the report.
  * Replace jquery.js and jquery-ui.js with Debian's system libraries again.

 -- Markus Koschany <apo@debian.org>  Wed, 16 Dec 2015 12:28:08 +0100

scala (2.11.6-3) unstable; urgency=medium

  * Team upload.
  * Avoid a source upload due to bug #805228.

 -- Markus Koschany <apo@debian.org>  Tue, 17 Nov 2015 15:22:53 +0100

scala (2.11.6-2) unstable; urgency=medium

  * Team upload.

  [ Emmanuel Bourg ]
  * Set the home directory when building to fix a build failure
    with pbuilder >= 0.220.

  [ Markus Koschany ]
  * Add local-repository.patch and define the localRepository variable
    explicitly, otherwise the /nonexistent home directory will be used which
    causes a FTBFS.
  * Fix Lintian warning missing-license-paragraph-in-dep5-copyright.
  * Add bnd-2.1.0.patch and make the package compatible with the latest version
    of bnd.

 -- Markus Koschany <apo@debian.org>  Mon, 16 Nov 2015 22:59:39 +0100

scala (2.11.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #706633, #750464, #760930)
    - Refreshed the patches
    - Added new dependencies on scala-xml and scala-parser-combinators
    - Removed 0001-Define-system-locations.patch (obsolete)
    - Removed 0005-java7-compilation.patch (fixed upstream)
    - Depend on libjline2-java instead of building an embedded copy of jline
    - Require a Java 7+ runtime
    - Refreshed debian/copyright
    - Adapted debian/orig-tar.sh for Scala 2.11
  * Preliminary changes to allow the installation of multiple version of Scala
    - Added a 'scala' alternative
    - Install the files under /usr/share/scala-2.11 and follow the layout
      of the upstream distribution
    - Added a /usr/share/scala link pointing to the default version
    - Added symlinks in /usr/share/java with the major Scala version
      (e.g. scala-library-2.11.jar)
    - Install the documentation under /usr/share/doc/scala-2.11.
  * Install the scaladoc of the reflection API
  * Removed the JRE dependency of the scala-library package
  * Build depend on scala (>= 2.11)
  * Install the Maven artifacts for scala-actors and scala-reflect
  * The package has been adopted by the Java Team (Closes: #754935)

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 19 Jun 2015 15:14:33 +0200

scala (2.10.5-1) unstable; urgency=low

  * Team upload.

  [ Mehdi Dogguy ]
  * New upstream release (Closes: #744278).

  [ Lucas Satabin ]
  * Update patches
  * Update the clean target
  * Update paths of elements to install
  * Update watch file

  [ Frank S. Thomas ]
  * Remove myself from Uploaders.

  [ Emmanuel Bourg ]
  * Patched the build to avoid downloading libraries from the Internet
  * Replaced the minified JavaScript files with unobfuscated ones
  * No longer build scala-partest.jar until diffutils is packaged or replaced
  * debian/watch: Fixed the versions matched (x.y.z instead of x.y.z..z)
  * debian/rules:
    - Added the missing get-orig-source target (Closes: #724704)
    - Improved the clean target
  * debian/control:
    - Build depend on scala (>= 2.10) and bnd
    - Use canonical URLs for the Vcs-* fields
    - Standards-Version updated to 3.9.6 (no changes)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 05 Jun 2015 23:52:59 +0200

scala (2.9.2+dfsg-2) unstable; urgency=low

  * Team upload.
  * Add patch to remove non-ASCII characters from source file
  * Build-dep on default-jdk instead of openjdk-6-jdk
    Add patch to build with JDK7.
    For scala and scala-library, update openjdk-6-jre dependency to 7.
    (Closes: #720569)
  * Bump Standards-Version to 3.9.5.
  * Remove deprecated DMUA flag.
  * Add lintian override for scala package description.

 -- tony mancill <tmancill@debian.org>  Thu, 26 Dec 2013 20:04:39 -0800

scala (2.9.2+dfsg-1) unstable; urgency=low

  [ Frank S. Thomas ]
  * New upstream release (LP: #987205).
    - Use "+dfsg" as suffix for the tarball (Closes: #641249).
  * Update debian/copyright to version 1.0 of the copyright format.
  * Bump standards version from 3.9.2 to 3.9.3.
  * Updated debian/watch to match the new names of the source tarballs
    (LP: #987561).

  [ Mehdi Dogguy ]
  * Release to unstable.

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 14 May 2012 13:48:14 +0200

scala (2.9.1.dfsg-3) unstable; urgency=low

  * Do not link to scala-library.jar in scala package (Closes: #654549).
  * Add jansi.jar to Scala's TOOL_CLASSPATH.

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 04 Jan 2012 11:46:39 +0100

scala (2.9.1.dfsg-2) unstable; urgency=low

  [ Frank S. Thomas ]
  * debian/copyright: Added missing Copyright fields and a License
    paragraph.

  [ Mehdi Dogguy ]
  * Build and install jline.jar, instead of relying on upstream's
    provided jline.jar (Closes: #652072).
    - Add junit4 and libjansi-java to Build-Depends.
    - Add libjansi-java to Scala's Depends field.
  * Provide Maven artifacts for scala-{library,compiler}, Thanks to
    Thomas Koch for the patch. (Closes: #652024).
  * Install scalacheck.jar and scala-partest.jar.
    - Mention it in scala-library's long description.
  * Add 0001-Use-system-ant-contrib.jar.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Wed, 28 Dec 2011 12:53:49 +0100

scala (2.9.1.dfsg-1) unstable; urgency=low

  [ Frank S. Thomas ]
  * Let scala suggest scala-doc (Closes: #632820).
  * Bump standards version to 3.9.2.
  * Add myself to Uploaders.
  * Use upstream's command scripts instead of the custom scripts in
    debian/command-scripts/.
    - These properly restore the terminal settings (Closes: #631771).
    - Added debian/patches/0002-Adapt-tool-unix.tmpl.patch to adapt
      the template that is used for the scripts to the layout of these
      packages.

  [ Mehdi Dogguy ]
  * New upstream release (Closes: #639960).
  * Use upstream's copy of jquery library. Scala-doc uses jQuery 1.4
    and is not compatible with 1.6 (yet), which got uploaded recently.
    (Closes: #636870).
    - Remove links created to system jquery{,ui}.js files
    - Remove dependencies of scala-doc on libjs-jquery{,ui}.

 -- Frank S. Thomas <fst@debian.org>  Wed, 07 Sep 2011 22:41:07 +0200

scala (2.9.0.1.dfsg-1) unstable; urgency=low

  [ Mehdi Dogguy ]
  * New upstream release (Closes: #631429) (LP: #782013).
  * Refresh patch.
  * Use Scala's provided JLine 2, instead of libjline-java.
  * Make scala-doc depends on libjs-jquery{,ui}.
  * Fix package description to make lintian happy.

  [ Frank S. Thomas ]
  * Updated debian/copyright for Scala 2.9.0.1.
  * Lintian recommends to refer to /u/s/common-licenses/GPL.
  * Fix spelling error in changelog.
  * Remove copyright information about docs/android-examples.

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 24 Jun 2011 14:31:05 +0200

scala (2.8.1.dfsg-1) unstable; urgency=low

  [ Mehdi Dogguy ]
  * New upstream release (Closes: #589110, #603529) (LP: #717745).
  * Use upstream build system (and re-write packaging).
  * Set a fixed version number (The one from META-INF/MANIFEST.MF).
  * Remove README{,.Debian}: they used to contain useless and outdated
    information.
  * Added a dependency on libjline-java for Scala.
  * Add libmaven-ant-tasks-java, ant-contrib and ant-optional in
    Build-Depends.
  * Switch to source format 3.0 (quilt).
  * Add patch 0001-Use-system-ant-contrib.jar.patch
  * Bump standards version to 3.9.1

  [ Frank S. Thomas ]
  * Update debian/copyright for the new release.

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 05 Mar 2011 20:41:44 +0100

scala (2.7.7.dfsg-4) unstable; urgency=low

  * Fix scaladoc script to use $CLASSPATH variable

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 23 Mar 2010 13:20:41 +0100

scala (2.7.7.dfsg-3) unstable; urgency=low

  * Fix scalac script (use $CLASSPATH variable), (LP: #544481)
  * Adjust the JRE/JVM depends (Closes: #573384)

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 23 Mar 2010 12:53:15 +0100

scala (2.7.7.dfsg-2) unstable; urgency=low

  * Fix scaladoc script (LP: #521093)

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 12 Feb 2010 21:15:06 +0100

scala (2.7.7.dfsg-1) unstable; urgency=low

  * New upstream release (Closes: #551454).

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 31 Oct 2009 21:16:38 +0100

scala (2.7.5.dfsg-2) unstable; urgency=low

  [ Min Huang ]
  * Do not compress script.js (Closes: #545051).

  [ Mehdi Dogguy ]
  * Use my Debian address.
  * Build-depend on debhelper 7.0.50 since we use overrides.

 -- Min Huang <min.huang@alumni.usc.edu>  Sun, 06 Sep 2009 12:54:53 +0200

scala (2.7.5.dfsg-1) unstable; urgency=low

  * New Upstream Version
  * Remove Lex Spoon from uploaders (retired), closes: #527957.
  * Shorten debian/rules: using new features of debhelper 7

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Tue, 02 Jun 2009 19:45:47 +0200

scala (2.7.4.dfsg-1) unstable; urgency=low

  * New Upstream Version
  * Add a watch file.
  * Add ${misc:Depends} as a dependency, thanks lintian.
  * Triggering doc-base.
  * Moving doc to /usr/share/doc/scala-doc/html/.
  * Bump standards version to 3.8.1.
  * Scala now depends on scala-library ${Source:Version}
  * Remove windows DLLs from upstream sources.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Fri, 24 Apr 2009 17:59:12 +0200

scala (2.7.3-3) unstable; urgency=low

  * Adding « java-virtual-machine » as an alternative runtime dependency
    (Closes: #464552).
  * Switch arch back to « all ».

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Mon, 02 Feb 2009 17:36:22 +0100

scala (2.7.3-2) unstable; urgency=low

  * Add Homepage field in debian/control
  * Fix debian/rules to execute binary-arch target.
  * Scala and Scala-library depend now on openjdk6-jre as an alternative
    to the virtual package java-virtual-machine, thanks Lintian.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Tue, 27 Jan 2009 11:20:34 +0100

scala (2.7.3-1) unstable; urgency=low

  [ Min Huang ]
  * New upstream release 
  * Crash with OpenJDK installed is fixed (Closes: #505913)

  [ Mehdi Dogguy ]
  * Set Maintainer to "Debian Scala Maintainers" mailing-list and Min 
    as Uploader.
  * Restrict "Architecture" field to build only where the java compiler
    exists (Closes: #483412).

 -- Lex Spoon <lex@debian.org>  Sat, 24 Jan 2009 20:32:34 -0500

scala (2.7.2-2) unstable; urgency=low

  * Recompiling to upload on Min Huang's behalf.

 -- Lex Spoon <lex@debian.org>  Sun, 30 Nov 2008 15:52:21 -0500

scala (2.7.2-1) unstable; urgency=low

  [ Min Huang ]
  * New upstream release.
  * Fixed out of memory error while building (Closes: #443040).
  * The pwd is added to the classpath when running scala.  Also, any
    command line arguments are passed to scala (Closes: #470667).

  [ Mehdi Dogguy ]
  * Using debhelper 7
  * Bump standards version to 3.8.0, no changes needed.
  * Relax build dependency by removing gcj from it.
  * Add a scala-doc package which install API html files.

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Sat, 15 Nov 2008 12:31:10 +0100

scala (2.7.1-1) unstable; urgency=low

  * New upstream release 

 -- Min Huang <min.huang@alumni.usc.edu>  Fri, 22 Aug 2008 00:57:33 -0700

scala (2.6.0-1) unstable; urgency=low

  * New upstream release

 -- Lex Spoon <lex@debian.org>  Fri, 14 Sep 2007 15:22:05 +0100

scala (2.5.0-1) unstable; urgency=low

  * New upstream release
  * Depend only on java-gcj-compat, not java-gcj-compat-dev

 -- Lex Spoon <lex@debian.org>  Tue, 22 May 2007 16:51:47 +0200

scala (2.4.0-1) unstable; urgency=low

  * New upstream version.

  * Deleting all mention of the Sun JVM, just because some
    DD's find it bothersome.

  * Deleted mention of JDK5-specific classes, while awaiting
    the Sun JVM to become open source.  (Closes: #409785)

  * Added java-gcj-compat-dev as an install dependency.  (Closes: #408353)

 -- Lex Spoon <lex@debian.org>  Sun, 25 Mar 2007 11:56:18 +0200

scala (2.3.2-1) unstable; urgency=low

  * New upstream version

 -- Lex Spoon <lex@debian.org>  Fri, 19 Jan 2007 14:45:51 +0100

scala (2.3.0-1) unstable; urgency=low

  * New upstream version
  * Added a Java compiler as a build-dependency

 -- Lex Spoon <lex@debian.org>  Fri, 24 Nov 2006 16:02:09 +0100

scala (2.2.0-1) unstable; urgency=low

  * New upstream release

 -- Lex Spoon <lex@debian.org>  Mon, 30 Oct 2006 21:35:10 -0500

scala (2.1.5-2) unstable; urgency=low

  * Rearranged to have three packages instead of four.

 -- Lex Spoon <lex@debian.org>  Wed, 28 Jun 2006 17:16:02 +0200

scala (2.1.5-1) unstable; urgency=low

  * upstream update.

 -- Lex Spoon <lex@debian.org>  Sun, 9 Jun 2006 14:45:26 +0100

scala (2.1.1-1) unstable; urgency=low

  * Initial Release.

 -- Lex Spoon <lex@debian.org>  Sun, 26 Mar 2006 14:45:26 +0100
